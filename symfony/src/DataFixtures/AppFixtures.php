<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use Faker;
use App\Entity\Message;
use App\Entity\Grade;

class AppFixtures extends Fixture
{
    private $faker;
    private $filesystem;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->faker = Faker\Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $this->loadUser($manager);
        $this->loadMessages($manager);
        $this->loadGrades($manager);
        $manager->flush();
    }
    private function loadUser(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $user->setPseudo($this->faker->word);
            $user->setRoles(['ROLE_USER']);
            $user->setUpvotes(random_int(0, 100));
            $user->setPassword($this->encoder->encodePassword($user, $user->getUsername()));
            $manager->persist($user);
            $this->addReference('user' . $i, $user);
        }
    }
    private function loadMessages(ObjectManager $manager)
    {
        for ($i = 0; $i < 40; $i++) {
            $message = new Message();
            $message->setDate($this->faker->dateTimeBetween());
            $message->setReceiver($this->getReference('user' . rand(0, 19)));
            $message->setSender($this->getReference('user' . rand(0, 19)));
            $message->setContent(implode(' ',$this->faker->sentences()));
            $manager->persist($message);
        }
    }
    private function loadGrades(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $grade = new Grade();
            $grade->setGrade(random_int(0, 20));
            if ($i % 3 === 0) {
                $grade->setComment(implode(' ', $this->faker->paragraphs()));
                $grade->setType('critic');
            }else {
                $grade->setType('comment');
            }
            $grade->setUser($this->getReference('user' . rand(0, 19)));
            $grade->setUpvotes(rand(0, 255));
            $manager->persist($grade);
        }
    }
}
