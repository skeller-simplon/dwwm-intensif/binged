import Axios from "axios";

export class Auth {
    constructor() {
        this.url = 'http://127.0.0.1:8000/api/'
    }
    async register(user) {
        let response = await Axios.post(this.url + 'register', user);
        if (response === 201) {
            return true;
        }
        return false;
    }
    async login(username, password) {
        let response = await Axios.post(this.url + 'login_check', { username: username, password: password });
        if (response.data) {
            localStorage.setItem('token', response.data.token);
            return true;
        }
        return false;
    }
}