<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;

class AuthController extends AbstractController
{
    /**
     * @Route("/api/register", methods="POST")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, ObjectManager $manager)
    {
        $user = new User;
        $form = $this->createForm(UserType::class, $user);
        $form->submit(json_decode(
            $request->getContent(),
            true
        ));
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $user->setRoles(['ROLE_USER']);
            $user->setUpvotes(0);
            $manager->persist($user);
            $manager->flush();
            return $this->json("", 201);
        }
        return $this->json($form->getErrors(true), 400);
    }
    /**
     * @Route ("/api/test/", methods="POST")
     */
    public function testAuth(){
        return $this->json(["message" => 'Connexion success - '. $this->getUser()->getPseudo()]);
    }
}
