import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios';

Vue.use(Vuex)

const apiUrl = 'http://localhost:8000';

export default new Vuex.Store({
  state: {
    user: null,
    shows: []
  },
  mutations: {
    CHANGE_USER(state, user) {
      state.user = user;
    },
    CHANGE_SHOWS(state, shows) {
      state.shows = shows;
    }
  },
  actions: {
    async login({ commit }, credentials) {
      let response = await Axios.post(apiUrl + '/api/login_check', credentials);
      localStorage.setItem('token', response.data.token);
      commit('CHANGE_USER', credentials.username);
    },
    logout({ commit }) {
      localStorage.removeItem('token');
      commit('CHANGE_USER', null);
    },
    async getAllShows({ commit }) {
      console.log('Bonsoir à tous');
      let response = await Axios.get("https://api.tvmaze.com/shows");
      console.log(response.data);
      commit('CHANGE_SHOWS', response.data);
    }
  },
  modules: {
  }
})
