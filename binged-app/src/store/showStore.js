import Axios from 'axios';

export default ({
  state: {
    shows: []
  },
  mutations: {
    GET_SHOWS(state, shows) {
      state.shows = shows;
    }
  },
  actions: {
    async getAll({ commit }) {
      let response = await Axios.get("http://api.tvmaze.com/shows");
      console.log(response.data);
      commit('GET_SHOWS', response.data);
    }
  }
})
