import Axios from "axios";

Axios.interceptors.request.use(function(config) { 
    if (config.url.match("^http:*/*localhost.{0,}")) {
        if( localStorage.getItem('token')){
            config.headers = {
                Authorization: 'Bearer '+localStorage.getItem('token')
            };
        }
    
    }
    return config;
});